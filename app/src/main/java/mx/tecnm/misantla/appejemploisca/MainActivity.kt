package mx.tecnm.misantla.appejemploisca

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        BtnEnviar.setOnClickListener {

            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()

           if(nombre.isEmpty()){
               Toast.makeText(this,"Debe ingresa su nombre",Toast.LENGTH_LONG).show()
               return@setOnClickListener
           }
            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresa su edad",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if(!checkBox.isChecked){
                Toast.makeText(this,"Debe aceptar los terminos",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            var genero = if(radioButton.isChecked)"Femenino" else "Masculino"

            val bundle = Bundle()
            bundle.apply{
                putString("key_nombre",nombre)
                putString("key_edad",edad)
                putString("key_sexo",genero)
            }


            val intent = Intent(this,SegundaActivity2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }
}