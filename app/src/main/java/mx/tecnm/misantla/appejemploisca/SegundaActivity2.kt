package mx.tecnm.misantla.appejemploisca

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_segunda2.*

class SegundaActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segunda2)

        val bundle : Bundle? = intent.extras

        bundle?.let {
            val name = it.getString("key_nombre")
            val age = it.getString("key_edad")
            val sex = it.getString("key_sexo")

            
            textView3.text = "Nombre : $name"
            textView4.text = "Edad : $age"
            textView5.text = "Genero : $sex"
        }

    }
}